/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package ca.hajjar.babypassport;

public final class R {
    public static final class attr {
    }
    public static final class color {
        public static final int bg_gray=0x7f040001;
        public static final int bg_green=0x7f040002;
        public static final int bottle_circle=0x7f040009;
        public static final int emphasis=0x7f040000;
        public static final int header_green=0x7f040003;
        public static final int header_text_color=0x7f040004;
        public static final int poo_circle=0x7f040007;
        public static final int section_color=0x7f040006;
        public static final int separator_color=0x7f04000a;
        public static final int sub_header_text_color=0x7f040005;
        public static final int wet_circle=0x7f040008;
    }
    public static final class dimen {
        /**  Default screen margins, per the Android Design guidelines. 

         Customize dimensions originally defined in res/values/dimens.xml (such as
         screen margins) for sw720dp devices (e.g. 10" tablets) in landscape here.
    
         */
        public static final int activity_horizontal_margin=0x7f050000;
        public static final int activity_vertical_margin=0x7f050001;
        public static final int add_log_size=0x7f050006;
        public static final int header_vertical_margin=0x7f050002;
        public static final int sub_title_vertical_margin=0x7f050004;
        public static final int sub_vertical_margin=0x7f050003;
        public static final int top_padding=0x7f050005;
    }
    public static final class drawable {
        public static final int actionbar_color=0x7f020000;
        public static final int baby_passport=0x7f020001;
        public static final int bottle=0x7f020002;
        public static final int bottle_icon_circle=0x7f020003;
        public static final int ic_action_accept=0x7f020004;
        public static final int ic_action_add_person=0x7f020005;
        public static final int ic_action_discard=0x7f020006;
        public static final int ic_action_discard_h=0x7f020007;
        public static final int ic_action_edit=0x7f020008;
        public static final int ic_action_new=0x7f020009;
        public static final int ic_action_new_event=0x7f02000a;
        public static final int ic_action_remove=0x7f02000b;
        public static final int ic_launcher=0x7f02000c;
        public static final int poo=0x7f02000d;
        public static final int poo_icon_circle=0x7f02000e;
        public static final int section_header=0x7f02000f;
        public static final int water=0x7f020010;
        public static final int wet_icon_circle=0x7f020011;
    }
    public static final class id {
        public static final int action_new_profile=0x7f09002f;
        public static final int add_bowel_type=0x7f090028;
        public static final int add_contact_action=0x7f09002a;
        public static final int add_feed_type=0x7f090027;
        public static final int add_log=0x7f09002e;
        public static final int add_wet_type=0x7f090029;
        public static final int babies_fragment=0x7f090000;
        public static final int baby_birth_weight=0x7f090009;
        public static final int baby_discharge_date=0x7f09000a;
        public static final int baby_discharge_weight=0x7f09000b;
        public static final int baby_dob=0x7f090007;
        public static final int baby_due_date=0x7f090008;
        public static final int baby_feeding_type=0x7f09000c;
        public static final int baby_name=0x7f090006;
        public static final int baby_name_list_item=0x7f090003;
        public static final int contact_address=0x7f090018;
        public static final int contact_address_edit=0x7f090022;
        public static final int contact_delete=0x7f090019;
        public static final int contact_edit=0x7f09001a;
        public static final int contact_name=0x7f090004;
        public static final int contact_name_edit=0x7f09001b;
        public static final int contact_phone=0x7f090017;
        public static final int contact_phone_edit=0x7f090021;
        public static final int contact_type=0x7f090005;
        public static final int contact_type_doctor=0x7f09001d;
        public static final int contact_type_edit=0x7f09001c;
        public static final int contact_type_midwife=0x7f09001f;
        public static final int contact_type_other=0x7f090020;
        public static final int contact_type_paediatrician=0x7f09001e;
        public static final int delete_action=0x7f09002b;
        public static final int edit_baby_info_action=0x7f09002c;
        public static final int edit_birth_weight=0x7f090010;
        public static final int edit_bottle=0x7f090014;
        public static final int edit_breast=0x7f090015;
        public static final int edit_dis_date=0x7f090011;
        public static final int edit_dis_weight=0x7f090012;
        public static final int edit_dob=0x7f09000e;
        public static final int edit_due_date=0x7f09000f;
        public static final int edit_feeding_types=0x7f090013;
        public static final int edit_name=0x7f09000d;
        public static final int log_date=0x7f090024;
        public static final int log_delete=0x7f090026;
        public static final int log_delete_action=0x7f09002d;
        public static final int log_icon=0x7f090023;
        public static final int log_list=0x7f090016;
        public static final int log_time=0x7f090025;
        public static final int pager=0x7f090001;
        public static final int pager_title_strip=0x7f090002;
        public static final int save_action=0x7f090030;
    }
    public static final class layout {
        public static final int activity_babies=0x7f030000;
        public static final int activity_main=0x7f030001;
        public static final int babies_item=0x7f030002;
        public static final int contact_item=0x7f030003;
        public static final int fragment_baby_info=0x7f030004;
        public static final int fragment_baby_info_edit=0x7f030005;
        public static final int fragment_baby_log=0x7f030006;
        public static final int fragment_baby_log_edit=0x7f030007;
        public static final int fragment_contact=0x7f030008;
        public static final int fragment_contact_edit=0x7f030009;
        public static final int log_item=0x7f03000a;
        public static final int log_types=0x7f03000b;
    }
    public static final class menu {
        public static final int add_contact_actions=0x7f080000;
        public static final int delete_action=0x7f080001;
        public static final int edit_action=0x7f080002;
        public static final int log_action=0x7f080003;
        public static final int logs_actions=0x7f080004;
        public static final int main=0x7f080005;
        public static final int save_action=0x7f080006;
    }
    public static final class string {
        public static final int action_settings=0x7f060001;
        public static final int add_action_title=0x7f060011;
        public static final int app_name=0x7f060000;
        public static final int baby_birth_weight_section=0x7f060008;
        public static final int baby_bottle_feed_section=0x7f06000c;
        public static final int baby_breast_feed_section=0x7f06000d;
        public static final int baby_dis_date_section=0x7f060009;
        public static final int baby_dis_weight_section=0x7f06000a;
        public static final int baby_dob_section=0x7f060006;
        public static final int baby_due_date_section=0x7f060007;
        public static final int baby_feeding_section=0x7f06000b;
        public static final int baby_info_title=0x7f06000f;
        public static final int baby_name_section=0x7f060005;
        public static final int baby_sex_section=0x7f06000e;
        public static final int call=0x7f06002a;
        public static final int cancel=0x7f060028;
        public static final int contact_address_section=0x7f060016;
        public static final int contact_name_section=0x7f060017;
        public static final int contact_phone_section=0x7f060015;
        public static final int contact_type_doctor=0x7f060019;
        public static final int contact_type_midwife=0x7f06001b;
        public static final int contact_type_other=0x7f06001c;
        public static final int contact_type_paediatrician=0x7f06001a;
        public static final int contact_type_section=0x7f060018;
        public static final int delete=0x7f060029;
        public static final int delete_contact_msg=0x7f06002f;
        public static final int delete_contact_title=0x7f06002e;
        public static final int delete_log_title=0x7f060030;
        public static final int delete_logs_msg=0x7f060031;
        public static final int delete_msg=0x7f06002c;
        public static final int delete_now=0x7f06002d;
        public static final int delete_title=0x7f06002b;
        public static final int done_title=0x7f060012;
        public static final int edit_action_title=0x7f060010;
        public static final int edit_date_title=0x7f060013;
        public static final int edit_time_title=0x7f060014;
        public static final int error_name_required=0x7f060020;
        public static final int feeding_type_bottle=0x7f060027;
        public static final int feeding_type_breast=0x7f060026;
        public static final int log_type=0x7f060032;
        public static final int log_type_bowel=0x7f060023;
        public static final int log_type_feed=0x7f060021;
        public static final int log_type_wet=0x7f060022;
        public static final int new_contact=0x7f06001f;
        public static final int new_profile=0x7f06001d;
        public static final int save_action_title=0x7f06001e;
        public static final int title_section1=0x7f060002;
        public static final int title_section2=0x7f060003;
        public static final int title_section3=0x7f060004;
        public static final int weight_type_kg=0x7f060025;
        public static final int weight_type_lb=0x7f060024;
    }
    public static final class style {
        /** 
        Base application theme, dependent on API level. This theme is replaced
        by AppBaseTheme from res/values-vXX/styles.xml on newer devices.
    

            Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can go here.
        

        Base application theme for API 11+. This theme completely replaces
        AppBaseTheme from res/values/styles.xml on API 11+ devices.
    
 API 11 theme customizations can go here. 

        Base application theme for API 14+. This theme completely replaces
        AppBaseTheme from BOTH res/values/styles.xml and
        res/values-v11/styles.xml on API 14+ devices.
    
 API 14 theme customizations can go here. 
         */
        public static final int AppBaseTheme=0x7f070000;
        /**  Application theme. 
 All customizations that are NOT specific to a particular API-level can go here. 
         */
        public static final int AppTheme=0x7f070001;
        public static final int MyActionBar=0x7f070003;
        public static final int MyTheme=0x7f070002;
    }
}
