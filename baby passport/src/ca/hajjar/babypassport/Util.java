package ca.hajjar.babypassport;

import ca.hajjar.babypassport.R;

import android.content.Context;
import android.widget.EditText;

public class Util {

	public static boolean validateEditText(Context context,EditText editText){
		boolean isValid = true;
		
		
		String text = editText.getText().toString();
		editText.setError(null);
		if(text == null || text.trim().isEmpty()){
			editText.setError(context.getString(R.string.error_name_required));
			isValid = false;
		}
		return isValid;
	}
}
